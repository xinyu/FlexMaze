﻿package xinyu.geom{
	public class Position2D{
		public var x:*;
		public var y:*;
		public var dir:*;
		
		public function Position2D(x:* = 0, y:* = 0, dir:* = 4){
			this.x = x;
			this.y = y;
			this.dir = dir;
		}
	}
}