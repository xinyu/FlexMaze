﻿/* Triangle.as */
package xinyu.geom{
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.display.GradientType;
	import flash.display.SpreadMethod;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	
	import xinyu.science.Science;

	public class Triangle extends Sprite{		
		private var _shape:Shape;
		private var _mass:Number;
				
		public function Triangle(innCol:int = 0xCCCCCC, outCol:int = 0x000000, length:Number = 40, degree:Number = 90){			
			_shape = new Shape();
			_mass = length/10;
			
			var fillType:String = GradientType.LINEAR;
  			var colors:Array = [innCol, outCol];
  			var alphas:Array = [1, 1];
  			var ratios:Array = [0x00, 0xAA];
  			var matr:Matrix = new Matrix();
  			var spreadMethod:String = SpreadMethod.PAD;
			  			
			var radian:Number = Science.degToRad(degree);
			
			matr.createGradientBox(length, length, radian, 0, 0);
  			_shape.graphics.beginGradientFill(fillType, colors, alphas, ratios, matr, spreadMethod);
			_shape.graphics.lineStyle(1,0x000000,1);
			_shape.graphics.moveTo(0, -length/2);
			_shape.graphics.lineTo(length/2, length/2);
			_shape.graphics.lineTo(-length/2, length/2);
			_shape.graphics.lineTo(0, -length/2);
			_shape.graphics.endFill();
			addChild(_shape);
		}
		
		public function get mass():Number{
			return _mass;
		}
	}
}
