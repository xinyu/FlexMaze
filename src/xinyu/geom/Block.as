﻿/* Block.as */
package xinyu.geom{
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.display.GradientType;
	import flash.display.SpreadMethod;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	
	import xinyu.science.Science;

	public class Block extends Sprite{		
		private var _shape:Shape;
		private var _mass:Number;
				
		public function Block(innCol:int = 0xffffff, outCol:int = 0x000000, width:Number = 40, height:Number = 40, degree:Number = 45){			
			_shape = new Shape();
			_mass = Math.sqrt(width*height)/10;
			
			var fillType:String = GradientType.LINEAR;
  			var colors:Array = [innCol, outCol];
  			var alphas:Array = [1, 1];
  			var ratios:Array = [0x00, 0xAA];
  			var matr:Matrix = new Matrix();
  			var spreadMethod:String = SpreadMethod.PAD;
			  			
			var radian:Number = Science.degToRad(degree);
			
			matr.createGradientBox(width, height, radian, 0, 0);
  			_shape.graphics.beginGradientFill(fillType, colors, alphas, ratios, matr, spreadMethod);  
			_shape.graphics.drawRect(0, 0, width, height);
			_shape.graphics.endFill();
			
			addChild(_shape);
		}
		
		public function get mass():Number{
			return _mass;
		}
	}
}
