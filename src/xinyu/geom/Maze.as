﻿package xinyu.geom{
	import flash.display.Sprite;
	
	import xinyu.geom.Block;
	import xinyu.geom.Position2D;
	import mx.core.UIComponent;
	
	public class Maze extends UIComponent{
		private var _maze:Array;
		private var _mObjs:Array;
		private var _row:uint;
		private var _col:uint;
		private var _start:Position2D;
		private var _end:Position2D;
		private var _blockSize:Number;
		
		public const WALL:int = 1;
		public const EMPTY:int = 0;
		public const PATH:int = 2;
		
		public function Maze(row:uint, col:uint, blockSize:Number){
			_start = new Position2D(8, 8);
			_end = new Position2D(1, 1);
			_row = row;
			_col = col;
			_blockSize = blockSize;
			
			_maze = new Array(_row);
			_mObjs = new Array(_row);
			for(var i:uint; i<_row; i++){
				_maze[i] = new Array(_col);
				_mObjs[i] = new Array(_col);
			}
			
			initMaze();
		}
		
		private function initMaze():void{
			
			_maze =	[	[1,1,1,1,1,1,1,1,1,1],
						[1,0,1,1,1,0,0,0,0,1],
						[1,0,0,1,0,0,1,1,1,1],
						[1,1,0,1,1,0,1,0,0,1],
						[1,0,0,0,0,0,1,1,0,1],
						[1,1,1,1,1,0,0,0,0,1],
						[1,0,1,0,0,0,1,1,1,1],
						[1,0,1,0,1,0,0,0,0,1],
						[1,0,0,0,1,1,1,1,0,1],
						[1,1,1,1,1,1,1,1,1,1]	];
			
			for(var i:uint=0; i<_row;i ++){
				for(var j:uint=0; j<_col; j++){
					_mObjs[i][j] = new Block(0x666666, 0x000000, _blockSize, _blockSize, 45);
					addChild(_mObjs[i][j]);
					_mObjs[i][j].alpha = 0;
					_mObjs[i][j].x = j * _blockSize;
					_mObjs[i][j].y = i * _blockSize;
				}
			}
			
			buildMaze();
		}
		
		public function buildMaze():void{
			for(var i:uint=0; i<_row;i++){
				for(var j:uint=0; j<_col; j++){
					if(_maze[i][j] == WALL){
						_mObjs[i][j].alpha = 1;
					}else{
						_mObjs[i][j].alpha = 0;
					}
				}
			}
		}
		
		public function isValid(pos:Position2D, dir:Position2D):int{
			var x:int = pos.x + dir.x;
			var y:int = pos.y + dir.y;
			if(x<0 || y<0 || x>= _col || y>= _row) return 1;
			return _maze[y][x];
		}
		
		public function get start():Position2D{
			return _start;
		}
		
		public function get end():Position2D{
			return _end;
		}
		
		public function setPath(pos:Position2D):void{
			if(_maze[pos.y][pos.x] != WALL){
				_maze[pos.y][pos.x] = PATH;
			}
		}
		
		public function cleanPath(pos:Position2D):void{
			if(_maze[pos.y][pos.x] != WALL){
				_maze[pos.y][pos.x] = EMPTY;
			}
		}
		
		public function reset():void{
			for(var i:uint = 0;i < _row; i++){
				for(var j:uint = 0; j < _col; j++){
					if(_maze[i][j] == PATH){
						_maze[i][j] = EMPTY;
					}
				}
			}
		}
		
		public function setAlpha(pos:Position2D, flag:Number):Boolean{
			if(pos.x !=0 && pos.x != _col-1 && pos.y != 0 && pos.y != _row-1){
				_maze[pos.y][pos.x] = flag;
				_mObjs[pos.y][pos.x].alpha = flag;
				return true;
			}else{
				return false;
			}
		}
	}
}