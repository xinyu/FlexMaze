﻿/* Ball.as */
package xinyu.geom{
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.display.GradientType;
	import flash.display.SpreadMethod;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	
	import xinyu.science.Science;

	public class Ball extends Sprite{		
		private var _shape:Shape;
		private var _mass:Number;
		private var _radius:Number;
				
		public function Ball(innCol:int = 0xffffff, outCol:int = 0x000000, radius:Number = 20, degree:Number = 0, offset:Number = 0){			
			_shape = new Shape();
			_mass = radius/10;
			_radius = radius;
			
			var fillType:String = GradientType.RADIAL;
  			var colors:Array = [innCol, outCol];
  			var alphas:Array = [1, 1];
  			var ratios:Array = [0x00, 0xAA];
  			var matr:Matrix = new Matrix();
  			var spreadMethod:String = SpreadMethod.PAD;
			  			
			var radian:Number = Science.degToRad(degree);
			
			matr.createGradientBox(4*radius, 4*radius, 0, 0.5*(radius+offset)*Math.cos(radian), 0.5*(radius+offset)*Math.sin(radian));
  			_shape.graphics.beginGradientFill(fillType, colors, alphas, ratios, matr, spreadMethod);  
			_shape.graphics.drawCircle(2*radius,2*radius,radius);
			_shape.graphics.endFill();
			
			_shape.x = -2*radius;
			_shape.y = -2*radius;
			addChild(_shape);
		}
		
		public function get mass():Number{
			return _mass;
		}
		
		public function get radius():Number{
			return _radius;
		}
	}
}
