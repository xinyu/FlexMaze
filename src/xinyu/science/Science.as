﻿/* Science */
package xinyu.science{
	public class Science{
		public function Science(){
		}
		
		public static function radToDeg(rad:Number):Number{
			return rad/Math.PI*180;
		}
		
		public static function degToRad(deg:Number):Number{
			return deg/180*Math.PI;
		}
		
		public static function randInt(from:int = 0, end:int = 100):int{
			if(from > end){
				var temp:int = end;
				end = from;
				from = temp;
			}
			return Math.round(Math.random()*(end-from))-Math.abs(from);			
		}
		
		public static function findMax(...args):*{
			var max:* = args[0];
			
			for(var i:int = 1; i <args.length ; i++){
				if(max < args[i]){
					max = args[i];
				}
			}
			return max;
		}
		
		public static function findMin(...args):*{
			var min:* = args[0];
			
			for(var i:int = 1; i <args.length ; i++){
				if(min > args[i]){
					min = args[i];
				}
			}
			return min;
		}
		
		public static function distance(x1:Number, y1:Number, x2:Number, y2:Number):Number{
			return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
		}
		
		public static function angleBetween(x1:Number, y1:Number, x2:Number, y2:Number):Number{
			return Math.atan2(y2-y1, x2-x1);
		}
	}
}