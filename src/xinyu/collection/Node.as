﻿package xinyu.collection{
	public class Node {
		public var next:Node = null;
		public var data:Object = null;
		
		public function Node(data:Object = null, next:Node = null){
			this.data = data;
			this.next = next;
		}
	}
}