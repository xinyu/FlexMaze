﻿/* Stack */
package xinyu.collection{
	import xinyu.collection.Node;
	public class Stack {
		private var _top:Node;
		private var _length:Number;
		
		public function Stack(){
			_top = null;
			_length = 0;
		}

		public function isEmpty():Boolean {
			return _top == null;
		}
		
		public function push(data : Object):void {
			var newTop:Node = new Node(data, _top);
			_top = newTop;
			_length += 1;
		}
		
		public function pop():Object {
			if (isEmpty ()) {
				return "empty";
			}
			
			_length -= 1;
			var data:Object = _top.data;
			_top = _top.next;
			return data;
		}
		
		public function peek():Object {
			if (isEmpty ()) {
				return "empty";
			}
			return _top.data;
		}
		
		public function get length():Number{
			return _length;
		}
		
		public function flush():void{
			_length = 0;
			_top = null;
		}
	}
}