﻿package xinyu.motion{
	public class Color{
		public static function interpolateColor(fromCol:uint, toCol:uint, percent:Number):uint{
			var r:uint = (fromCol & 0xff0000) >> 16;
			var g:uint = (fromCol & 0x00ff00) >> 8;
			var b:uint = (fromCol & 0x0000ff);
			var rDiff:int = (((fromCol & 0xff0000) >> 16) -  ((toCol & 0xff0000) >> 16));
			var gDiff:int = (((fromCol & 0x00ff00) >> 8) -  ((toCol & 0x00ff00) >> 8));
			var bDiff:int = ((fromCol & 0x0000ff) -  (toCol & 0x0000ff));
			return ( ( (r - rDiff * percent) << 16) + ( (g - gDiff * percent) << 8) + (b - bDiff * percent) );
		}
	}
}