﻿package xinyu.utils{
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GradientBevelFilter;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import mx.controls.TextArea;
	import mx.core.UIComponent;
	
	import xinyu.button.FlexSymButton;
	
	public class WindowPanel extends UIComponent{
		private var _closeBtn:FlexSymButton;
		private var _title:TextField;
		private var _msg:TextArea;
		private var _titleFmt:TextFormat;
		private var _titleBar:Shape;
		private var _outShape:Shape;
		private var _innShape:Shape;
		
		public function WindowPanel(title:String = "", msg:String = "", width:Number = 320, height:Number = 240, alpha:Number = 0.85, color:int = 0x8DABCB){
			_outShape = new Shape();
			addChild(_outShape);
			_outShape.graphics.beginFill(color);
			_outShape.graphics.drawRoundRect(0, 0, width, height, 10);
			_outShape.graphics.endFill();
			_outShape.filters = [new DropShadowFilter()];
			_outShape.alpha = alpha;
			
			_innShape = new Shape();
			_innShape.graphics.beginFill(0xffffff);
			_innShape.graphics.drawRect(0,0,width-20, height-40);
			_innShape.graphics.endFill();
			_innShape.filters = [new GradientBevelFilter(2,45,[0xCCCCCC, 0xffffff, 0x666666], [1,1,1], [0,128,255], 2, 2, 1, 1)];
			
			this.addChild(_innShape);
			_innShape.x = 10;
			_innShape.y = 30;
			
			_titleBar = new Shape();
			_titleBar.graphics.beginFill(color);
			_titleBar.graphics.drawRect(0,0,width-20, 20);
			_titleBar.graphics.endFill();
			_titleBar.filters = [new GradientBevelFilter(1,45,[0xffffff, color], [0.5, 0.5], [0,255] , 1, 1, 1, 1)];
			
			this.addChild(_titleBar);
			_titleBar.x = 10;
			_titleBar.y = 5;			
			
			_titleFmt = new TextFormat();
			_titleFmt.bold =  true;
			_titleFmt.size = 12;
			_titleFmt.align = TextFormatAlign.LEFT;
			_titleFmt.font = "Verdana";
			
			_title = new TextField();
			addChild(_title);
			_title.width = width - 24;
			_title.x = 12;
			_title.y = 5;
			_title.text = title;
			_title.textColor = 0xffffff;
			_title.selectable = false;
			_title.setTextFormat(_titleFmt);
			
			_msg = new TextArea();
			addChild(_msg);
			_msg.width = width - 20;
			_msg.height = _innShape.height; 
			_msg.x = 12;
			_msg.y = 32;
			_msg.htmlText = msg;
			_msg.editable = false;
			
			_closeBtn = new FlexSymButton(20, 2, 2, color, FlexSymButton.CLOSE);
			addChild(_closeBtn);
			_closeBtn.x = width-30;
			_closeBtn.y = 5;
			_closeBtn.addEventListener(MouseEvent.CLICK, onCloseClicked);
		}
		
		private function onCloseClicked(event:MouseEvent):void{
			event.target.parent.parent.visible = false;
		}
	}
}